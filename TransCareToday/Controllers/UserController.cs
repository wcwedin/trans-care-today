﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TransCareToday.Models;

namespace TransCareToday.Controllers
{
	[ApiController]
	public class UserController : ControllerBase
	{
		private readonly TransCareTodayContext context;
		private readonly SignInManager<User> signInManager;
		private readonly UserManager<User> userManager;
		private readonly ILogger<UserController> logger;
		private readonly IEmailSender emailSender;

		public UserController(TransCareTodayContext context, UserManager<User> userManager, SignInManager<User> signInManager, ILogger<UserController> logger, IEmailSender emailSender)
		{
			this.context = context;
			this.userManager = userManager;
			this.signInManager = signInManager;
			this.logger = logger;
			this.emailSender = emailSender;
		}

		[TempData]
		public string StatusMessage { get; set; }

		public class UserInputModel
		{
			[Required]
			[EmailAddress]
			[Display(Name = "Email")]
			public string Email { get; set; }

			[Required]
			[StringLength(44, ErrorMessage = "The {0} must be exactly 44 characters long.", MinimumLength = 44)]
			[DataType(DataType.Password)]
			[Display(Name = "Hashed Password")]
			public string HashedPassword { get; set; }
		}

		public class ChangePasswordInputModel
		{
			[Required]
			[StringLength(44, ErrorMessage = "The {0} must be exactly 44 characters long.", MinimumLength = 44)]
			[DataType(DataType.Password)]
			[Display(Name = "Old Hashed Password")]
			public string OldHashedPassword { get; set; }

			[Required]
			[StringLength(44, ErrorMessage = "The {0} must be exactly 44 characters long.", MinimumLength = 44)]
			[DataType(DataType.Password)]
			[Display(Name = "New Hashed Password")]
			public string NewHashedPassword { get; set; }
		}

		public class SignInInputModel
		{
			[Required]
			[EmailAddress]
			public string Email { get; set; }

			[Required]
			[StringLength(44, ErrorMessage = "The {0} must be exactly 44 characters long.", MinimumLength = 44)]
			[DataType(DataType.Password)]
			[Display(Name = "New Hashed Password")]
			public string HashedPassword { get; set; }

			[Display(Name = "Remember me?")]
			public bool RememberMe { get; set; }
		}

		[Route("api/[controller]/lockbox")]
		[HttpGet]
		public async Task<IActionResult> GetLockbox()
		{
			var user = await userManager.GetUserAsync(User);
			if (user == null)
			{
				ModelState.AddModelError(string.Empty, $"Unable to load user with ID '{userManager.GetUserId(User)}'.");
				return BadRequest(ModelState);
			}
			else
			{
				var lockbox = context.Lockboxes.Find(user.LockboxId);
				return new JsonResult(lockbox);
			}
		}

		[AllowAnonymous]
		[Route("api/[controller]/challenge")]
		[HttpPost]
		public async Task<IActionResult> GetChallenge([FromForm] string email)
		{
			var user = await userManager.FindByNameAsync(email);
			if (user == null)
			{
				ModelState.AddModelError(string.Empty, $"Unable to load user with email adddress {email}.");
				return BadRequest(ModelState);
			}
			else
			{
				var lockbox = context.Lockboxes.Find(user.LockboxId);
				var challenge = new
				{
					lockbox.PasswordHashFunction,
					lockbox.PasswordIterationCount,
					lockbox.PasswordSalt
				};
				return new JsonResult(challenge);
			}
		}

		[AllowAnonymous]
		[Route("api/[controller]/sign-in")]
		[HttpPost]
		public async Task<IActionResult> SignIn([FromForm] SignInInputModel input)
		{
			var result = await signInManager.PasswordSignInAsync(input.Email, input.HashedPassword, input.RememberMe, lockoutOnFailure: true);
			if (result.Succeeded)
			{
				logger.LogInformation("User logged in.");
				return new JsonResult(new { });
			}
			if (result.RequiresTwoFactor)
			{
				ModelState.AddModelError(string.Empty, "Two-factor authentication required.");
			}
			if (result.IsLockedOut)
			{
				logger.LogWarning("User account locked out.");
				ModelState.AddModelError(string.Empty, "User account locked.");
			}
			else
			{
				ModelState.AddModelError(string.Empty, "Invalid login attempt.");
			}
			return BadRequest(ModelState);
		}

		[Route("api/[controller]/change-password")]
		[HttpPost]
		public async Task<IActionResult> ChangePassword([FromForm] ChangePasswordInputModel changePasswordInput, [FromForm] Lockbox lockbox)
		{
			if (ModelState.IsValid)
			{
				var user = await userManager.GetUserAsync(User);
				if (user == null)
				{
					ModelState.AddModelError(string.Empty, $"Unable to load user with ID '{userManager.GetUserId(User)}'.");
					return BadRequest(ModelState);
				}

				user.Lockbox = lockbox;
				using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
				{
					var changePasswordResult = await userManager.ChangePasswordAsync(user, changePasswordInput.OldHashedPassword, changePasswordInput.NewHashedPassword);
					var updateResult = await userManager.UpdateAsync(user);
					if (changePasswordResult.Succeeded && updateResult.Succeeded)
					{
						await signInManager.RefreshSignInAsync(user);
						logger.LogInformation("User changed their password successfully.");
						StatusMessage = "Your password has been changed.";
						transaction.Complete();
						return new JsonResult(new { });
					}

					foreach (var error in changePasswordResult.Errors)
					{
						ModelState.AddModelError(string.Empty, error.Description);
					}

					foreach (var error in updateResult.Errors)
					{
						ModelState.AddModelError(string.Empty, error.Description);
					}
				}
			}

			return BadRequest(ModelState);
		}

		// POST: api/User
		[AllowAnonymous]
		[Route("api/[controller]")]
		[HttpPost]
		public async Task<IActionResult> Post([FromForm] UserInputModel user, [FromForm] Lockbox lockbox, string returnUrl = null)
		{
			returnUrl = returnUrl ?? Url.Content("~/");
			if (ModelState.IsValid)
			{
				var newUser = new User { UserName = user.Email, Email = user.Email };
				newUser.Lockbox = lockbox;
				var result = await userManager.CreateAsync(newUser, user.HashedPassword);
				if (result.Succeeded)
				{
					logger.LogInformation("User created a new account with password.");

					var code = await userManager.GenerateEmailConfirmationTokenAsync(newUser);
					var callbackUrl = Url.Page(
						"/Account/ConfirmEmail",
						pageHandler: null,
						values: new { userId = newUser.Id, code = code },
						protocol: Request.Scheme);

					await emailSender.SendEmailAsync(user.Email, "Confirm your email",
						$"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

					await signInManager.SignInAsync(newUser, isPersistent: false);
					return new JsonResult(new { returnUrl });
				}
				foreach (var error in result.Errors)
				{
					ModelState.AddModelError(string.Empty, error.Description);
				}
			}

			return BadRequest(ModelState);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using TransCareToday.Models;
using TransCareToday.ViewModels.TableView;

namespace TransCareToday.Controllers
{
    public class ProvidersController : Controller
    {
        private readonly TransCareTodayContext _context;

        public ProvidersController(TransCareTodayContext context)
        {
            _context = context;
        }
		
		public async Task<IActionResult> Index(int? pageIndex, int? pageSize, int? oldPageSize, string sortColumnsJson, string filterColumnsJson)
		{
			var providers = _context.Providers;

			pageIndex = pageIndex ?? 1;

			if (pageSize.HasValue && oldPageSize.HasValue && pageSize != oldPageSize)
			{
				int rowIndex = (pageIndex.Value - 1) * oldPageSize.Value;
				pageIndex = rowIndex / pageSize.Value + 1;
			}

			var parameters = new TableViewParameters
			{
				PageIndex = pageIndex.Value,
				PageSize = pageSize ?? 10
			};

			List<SortColumn> sortColumns = null;
			try { sortColumns = JsonConvert.DeserializeObject<List<SortColumn>>(sortColumnsJson); } catch { }
			if (sortColumns != null) parameters.SortColumns.AddRange(sortColumns);

			List<FilterColumn> filterColumns = null;
			try { filterColumns = JsonConvert.DeserializeObject<List<FilterColumn>>(filterColumnsJson); } catch { }
			if (filterColumns != null) parameters.FilterColumns.AddRange(filterColumns);

			return View(await providers.ToTableView(parameters));
		}

		// GET: Providers/Details/5
		public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var provider = await _context.Providers
                .FirstOrDefaultAsync(m => m.ProviderId == id);
            if (provider == null)
            {
                return NotFound();
            }

            return View(provider);
        }

        // GET: Providers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Providers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProviderId,IsInactive,IsAcceptingNewPatients,Name,Description,AddressLine1,AddressLine2,AddressLine3,City,State,ZipCode,SearchVector")] Provider provider)
        {
            if (ModelState.IsValid)
            {
                provider.ProviderId = Guid.NewGuid();
                _context.Add(provider);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(provider);
        }

        // GET: Providers/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var provider = await _context.Providers.FindAsync(id);
            if (provider == null)
            {
                return NotFound();
            }
            return View(provider);
        }

        // POST: Providers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("ProviderId,IsInactive,IsAcceptingNewPatients,Name,Description,AddressLine1,AddressLine2,AddressLine3,City,State,ZipCode,SearchVector")] Provider provider)
        {
            if (id != provider.ProviderId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(provider);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProviderExists(provider.ProviderId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(provider);
        }

        // GET: Providers/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var provider = await _context.Providers
                .FirstOrDefaultAsync(m => m.ProviderId == id);
            if (provider == null)
            {
                return NotFound();
            }

            return View(provider);
        }

        // POST: Providers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var provider = await _context.Providers.FindAsync(id);
            _context.Providers.Remove(provider);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProviderExists(Guid id)
        {
            return _context.Providers.Any(e => e.ProviderId == id);
        }
    }
}

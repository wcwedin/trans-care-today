﻿function initSurveySubmit(button) {
	let form = button.closest("form");
	form.submit(function (e) {
		e.preventDefault();
		let form = $(this);
		let data = parseSurvey(form);
	});
	button.attr("type", "submit");
}

function parseSurvey(form) {
	let fields = form.find("input, textarea, select");
	let result = {};
	for (let field of fields) {
		field = $(field);
		result[field.attr("name")] = field.val();
	}
}

function parseEditor(form) {
	let formSections = form.find("section:not([data-remove='true'])");

	let sections = [];
	for (let formSection of formSections) {
		formSection = $(formSection);
		let fieldsets = formSection.find("fieldset:not([data-remove='true'])");
		let name = formSection.find(">label>input").val();
		let questions = [];
		for (let fieldset of fieldsets) {
			fieldset = $(fieldset);
			let object = {};
			for (let input of fieldset.find("input")) {
				input = $(input);
				object[input.attr("name")] = input.val();
			}
			let question = SurveyItem.deserialize(object);
			questions.push(question);
		}
		let section = new Section(questions, name);
		sections.push(section);
	}
	let survey = new Survey(sections, []);
	return survey;
}

function submitEditor(e) {
	e.preventDefault();
	let button = $(this);
	let form = button.closest("form");
	let data = parseEditor(form);
}

function initEditor(form) {
	form.submit(submitEditor);
	let sections = form.find(">div.sections");
	sections.sortable();
	sections.disableSelection();

	let addSection = function () {
		let editor = new Section([], "").renderEditor();
		sections.append(editor);
		editor.find("button.add-question").click(addQuestion);
		editor.find("button.remove-section").click(removeSection);
	};

	let removeSection = function () {
		let button = $(this);
		let section = button.closest("section");
		section.attr("data-remove", true);
		section.find("input, select, button").prop("disabled", true);

		button.prop("disabled", false);
		button.removeClass("remove-section");
		button.addClass("restore-section");
		button.html("Restore section");
		button.off("click");
		button.click(restoreSection);
	};

	let restoreSection = function () {
		let button = $(this);
		let section = button.closest("section");
		section.attr("data-remove", false);
		section.find("input, select, button").prop("disabled", false);
		section.find("fieldset[data-remove='true'] input, fieldset[data-remove='true'] select").prop("disabled", true);

		button.removeClass("restore-section");
		button.addClass("remove-section");
		button.html("Remove section");
		button.off("click");
		button.click(removeSection);
	};

	let addQuestion = function () {
		let buttonsSection = $(this).closest(".buttons");
		let section = $(this).closest("section");
		let editor;
		let type = buttonsSection.find(".add-question-type").val();
		switch (type) {
			case "text":
				editor = new TextQuestion("", "", false, null, null, null, null).renderEditor();
				break;
			case "email":
				editor = new EmailQuestion("", "", false, null, null, null, null).renderEditor();
				break;
			case "tel":
				editor = new TelQuestion("", "", false, null, null, null, null).renderEditor();
				break;
			case "number":
				editor = new NumberQuestion("", "", false, null, null, null, null).renderEditor();
				break;
			case "range":
				editor = new RangeQuestion("", "", false, null, null, null, null).renderEditor();
				break;
			case "date":
				editor = new DateQuestion("", "", false, null, null, null, null).renderEditor();
				break;
			case "time":
				editor = new TimeQuestion("", "", false, null, null, null, null).renderEditor();
				break;
		}
		editor.find("button.remove-question").click(removeQuestion);
		section.find(".questions").append(editor);
	};

	let removeQuestion = function () {
		let button = $(this);
		let question = button.closest("fieldset");
		question.attr("data-remove", true);
		question.find("input, select, button").prop("disabled", true);

		button.prop("disabled", false);
		button.removeClass("remove-question");
		button.addClass("restore-question");
		button.html("Restore question");
		button.off("click");
		button.click(restoreQuestion);
	};

	let restoreQuestion = function () {
		let button = $(this);
		let question = button.closest("section");
		question.attr("data-remove", false);
		question.find("input, select, button").prop("disabled", false);

		button.removeClass("restore-question");
		button.addClass("remove-question");
		button.html("Remove question");
		button.off("click");
		button.click(removeQuestion);
	};

	form.find("button.add-section").click(addSection);

	form.find("button.remove-section").click(removeSection);

	form.find("button.restore-section").click(restoreSection);

	form.find("button.add-question").click(addQuestion);

	form.find("button.remove-question").click(removeQuestion);

	form.find("button.restore-question").click(restoreQuestion);

	form.find("input.name").on("invalid", function (event) {
		event.target.setCustomValidity('Field names can only contain letters, numbers, and hyphens.');
	});

	let submitButton = form.find("button.submit");
	submitButton.attr("type", "submit");
}

class SurveyItem {
	constructor() {
		if (this.constructor === SurveyItem) throw TypeError("new of abstract class SurveyItem");
		this.class = this.constructor.name;
	}

	static deserialize(object) {
		if (object.class === Survey.name) return Survey.deserialize(object);
		if (object.class === Section.name) return Section.deserialize(object);
		if (object.class === Group.name) return Group.deserialize(object);

		if (object.class === NumberQuestion.name || object.type === "number") return NumberQuestion.deserialize(object);
		if (object.class === DateQuestion.name || object.type === "date") return DateQuestion.deserialize(object);
		if (object.class === TimeQuestion.name || object.type === "time") return TimeQuestion.deserialize(object);
		if (object.class === RangeQuestion.name || object.type === "range") return RangeQuestion.deserialize(object);

		if (object.class === EmailQuestion.name || object.type === "email") return EmailQuestion.deserialize(object);
		if (object.class === TelQuestion.name || object.type === "tel") return TelQuestion.deserialize(object);
		if (object.class === TextQuestion.name || object.type === "text") return TextQuestion.deserialize(object);
	}
}

class Survey extends SurveyItem {
	constructor(children, dataLists) {
		super();
		this.children = children;
		this.dataLists = dataLists;
	}

	static deserialize(object) {
		let children = [];
		for (let child of object.children) {
			children.push(SurveyItem.deserialize(child));
		}
		let dataLists = [];
		for (let dataList of object.dataLists) {
			dataLists.push(SurveyItem.deserialize(dataList));
		}
		return new Survey(children, dataLists);
	}

	render(values, readonly) {
		let form = $("<form>");
		form.addClass("survey");

		for (let child of this.children) {
			var render = child.render(values, readonly);
			form.append(render);
		}

		let button = $("<button>");
		button.attr("type", "button");
		button.append("Submit");
		form.append(button);
		initSurveySubmit(button);

		return form;
	}

	renderEditor() {
		let form = $("<form>");
		form.addClass("editor");

		let div = $("<div>");
		div.addClass("sections");

		for (let child of this.children) {
			div.append(child.renderEditor());
		}

		form.append(div);

		let buttons = $("<div>");
		buttons.addClass("buttons");

		let addButton = $("<button>");
		addButton.attr("type", "button");
		addButton.addClass("add-section");
		addButton.append("Add section");
		buttons.append(addButton);

		let submitButton = $("<button>");
		submitButton.attr("type", "button");
		submitButton.addClass("submit");
		submitButton.append("Submit");
		buttons.append(submitButton);

		form.append(buttons);

		initEditor(form);
		return form;
	}
}

class Section extends SurveyItem {
	constructor(children, name) {
		super();
		this.children = children;
		this.name = name;
	}

	static deserialize(object) {
		let children = [];
		for (let child of object.children) {
			children.push(SurveyItem.deserialize(child));
		}
		return new Section(children, object.name);
	}

	render(values, readonly) {
		let fieldset = $("<fieldset>");
		let legend = $("<legend>");
		legend.append(this.name);
		fieldset.append(legend);
		for (let child of this.children) {
			fieldset.append(child.render(values, readonly));
		}
		return fieldset;
	}

	renderEditor() {
		let section = $("<section>");

		section.append($("<label>")
			.append("Section name")
			.append($("<input>")
				.attr("name", "section-name")
				.attr("type", "text")
				.attr("value", this.name)
			)
		);

		let div = $("<div>");
		div.addClass("questions");

		for (let child of this.children) {
			div.append(child.renderEditor());
		}

		div.sortable();
		div.disableSelection();

		section.append(div);

		let buttons = $("<div>");
		buttons.addClass("buttons");

		let select = $("<select>");
		select.addClass("add-question-type");
		select.append($("<option>").attr("value", "text").append("Text"));
		select.append($("<option>").attr("value", "email").append("Email"));
		select.append($("<option>").attr("value", "tel").append("Telephone"));
		select.append($("<option>").attr("value", "number").append("Number"));
		select.append($("<option>").attr("value", "range").append("Range"));
		select.append($("<option>").attr("value", "date").append("Date"));
		select.append($("<option>").attr("value", "time").append("Time"));
		let type = $("<label>");
		type.append("Question type");
		type.append(select);
		buttons.append(type);

		let addButton = $("<button>");
		addButton.attr("type", "button");
		addButton.addClass("add-question");
		addButton.append("Add question");
		buttons.append(addButton);

		let removeButton = $("<button>");
		removeButton.attr("type", "button");
		removeButton.addClass("remove-section");
		removeButton.append("Remove section");
		buttons.append(removeButton);

		section.append(buttons);

		return section;
	}
}

class Group extends SurveyItem {
	constructor(children) {
		super();
		this.children = children;
	}

	static deserialize(object) {
		let children = [];
		for (let child of object.children) {
			children.push(SurveyItem.deserialize(child));
		}
		return new Group(children);
	}

	render() {
		let div = $("<div>");
		for (let child of this.children) {
			div.append(child.render());
		}
		return div;
	}

	renderEditor() {
		let div = $("<div>");
		div.addClass("group");
		for (let child of this.children) {
			div.append(child.renderEditor());
		}
		return div;
	}
}

class DataListOption extends SurveyItem {
	constructor(value) {
		super();
		this.value = value;
	}

	render() {
		let option = $("<option>");
		option.prop("value", this.value);
		return option;
	}
}

class DataList extends SurveyItem {
	constructor(name, items) {
		super();
		this.name = name;
		this.items = items;
	}

	render() {
		let dataList = $("<datalist>");
		datalist.attr("id", this.name);
		for (let item of this.items) {
			dataList.append(item.render());
		}
		return dataList;
	}
}

class Question extends SurveyItem {
	constructor(name, label, required) {
		super();
		this.name = name;
		this.label = label;
		this.required = !!required;
	}

	render(values, readonly) {
		let span = $("<span>");

		let label = $("<label>");
		label.attr("for", this.name);
		label.append(this.label);
		span.append(label);

		let input = this.renderInput();
		if (values) {
			input.val(values[this.name]);
		}
		if (readonly) {
			input.attr("readonly", true);
		}
		span.append(input);
		return span;
	}

	renderEditor() {
		let fieldset = $("<fieldset>");

		let removeButton = $("<button>");
		removeButton.attr("type", "button");
		removeButton.addClass("remove-question");
		removeButton.append("Remove question");
		fieldset.append(removeButton);

		fieldset.append($("<input>")
			.attr("name", "type")
			.attr("type", "hidden")
			.attr("value", this.type)
			.addClass("type")
		);
		fieldset.append($("<label>")
			.append("Name")
			.append($("<input>")
				.attr("name", "name")
				.attr("type", "text")
				.attr("required", true)
				.attr("pattern", "[0-9A-Za-z-]+")
				.attr("value", this.name)
				.addClass("name")
			)
		);
		fieldset.append($("<label>")
			.append("Label")
			.append($("<input>")
				.attr("name", "label")
				.attr("type", "text")
				.attr("required", true)
				.prop("value", this.label)
			)
		);
		fieldset.append($("<label>")
			.append("Required")
			.append($("<input>")
				.attr("name", "required")
				.attr("type", "checkbox")
				.prop("checked", this.required)
			)
		);
		return fieldset;
	}
}

class SelectQuestion extends Question {
	constructor(name, label, required, items, multiple, renderAsBoxes, size) {
		super(name, label, required);
		this.items = items;
		this.multiple = multiple;
		this.renderAsBoxes = renderAsBoxes;
		this.size = size;
	}

	renderEditor() {
		let fieldset = super.renderEditor();
		// TODO: Stuff for items.
		fieldset.append($("<label>")
			.append("Multiselect")
			.append($("<input>")
				.attr("name", "multiple")
				.attr("type", "checkbox")
				.prop("checked", this.multiple)
			)
		);

		fieldset.append($("<label>")
			.append("Use checkboxes")
			.append($("<input>")
				.attr("name", "renderAsBoxes")
				.attr("type", "checkbox")
				.prop("checked", this.renderAsBoxes)
			)
		);

		fieldset.append($("<label>")
			.append("Size")
			.append($("<input>")
				.attr("name", "size")
				.attr("type", "number")
				.prop("value", this.size)
			)
		);

		return fieldset;
	}

	renderAsSelect() {
		let select = $("<select>");
		select.attr("name", this.name);
		select.attr("id", this.name);
		select.prop("multiple", this.multiple);
		for (let item of this.items) {
			let option = $("<option>");
			option.append(item.label);
			option.prop("value", item.value);
			select.append(option);
		}
		return select;
	}

	renderAsCheckboxes() {
		let div = $("<div>");
		for (let item of this.items) {
			let span = $("<span>");

			let checkbox = $("<input>");
			checkbox.attr("type", "checkbox");
			checkbox.attr("name", this.name);
			checkbox.attr("id", this.name + "-" + item.value);
			checkbox.prop("value", item.value);
			span.append(checkbox);

			let label = $("label");
			label.prop("for", this.name + "-" + item.value);
			label.append(item.label);
			span.append(label);

			div.append(span);
		}
		return div;
	}

	renderAsRadioButtons() {
		let div = $("<div>");
		for (let item of this.items) {
			let span = $("<span>");

			let checkbox = $("<input>");
			checkbox.attr("type", "radio");
			checkbox.attr("name", this.name);
			checkbox.attr("id", this.name + "-" + item.value);
			checkbox.prop("value", item.value);
			span.append(checkbox);

			let label = $("label");
			label.prop("for", this.name + "-" + item.value);
			label.append(item.label);
			span.append(label);

			div.append(span);
		}
		return div;
	}

	renderInput() {
		if (this.renderAsBoxes) {
			if (this.multiple) {
				return this.renderAsRadioButtons();
			} else {
				return this.renderAsCheckboxes();
			}
		} else {
			return this.renderAsSelect();
		}
	}
}

class InputQuestion extends Question {
	constructor(name, label, required, list) {
		super(name, label, required);
		this.list = list;
	}

	renderInput() {
		let input = $("<input>");
		input.attr("type", this.type);
		input.attr("id", this.name);
		if (this.required) input.attr("required", this.required);
		return input;
	}

	renderEditor() {
		let fieldset = super.renderEditor();
		// TODO: Stuff for list.
		return fieldset;
	}
}

class NumericQuestion extends InputQuestion {
	constructor(name, label, required, list) {
		super(name, label, required, list);
	}

	renderInput() {
		let input = super.renderInput();
		return input;
	}
}

class NumberQuestion extends NumericQuestion {
	constructor(name, label, required, list, min, max, step) {
		super(name, label, required, list);
		this.min = min * 1;
		this.max = max * 1;
		this.step = step * 1;
	}

	get type() { return "number"; }

	static deserialize(object) {
		return new NumberQuestion(object.name, object.label, object.required, object.list, object.min, object.max, object.step);
	}

	renderEditor() {
		let fieldset = super.renderEditor();
		fieldset.append($("<label>")
			.append("Minimum")
			.append($("<input>")
				.attr("name", "min")
				.attr("type", "number")
			)
		);
		fieldset.append($("<label>")
			.append("Maximum")
			.append($("<input>")
				.attr("name", "max")
				.attr("type", "number")
			)
		);
		fieldset.append($("<label>")
			.append("Step")
			.append($("<input>")
				.attr("name", "step")
				.attr("type", "number")
			)
		);
		return fieldset;
	}

	renderInput() {
		let input = super.renderInput();
		if (this.min) input.attr("min", this.min);
		if (this.max) input.attr("max", this.max);
		if (this.step) input.attr("step", this.step);
		return input;
	}
}

class DateQuestion extends NumericQuestion {
	constructor(name, label, required, list, min, max, step) {
		super(name, label, required, list);
		this.min = min;
		this.max = max;
		this.step = step * 1;
	}

	get type() { return "date"; }

	static deserialize(object) {
		return new DateQuestion(object.name, object.label, object.required, object.list, object.min, object.max, object.step);
	}

	renderEditor() {
		let fieldset = super.renderEditor();
		fieldset.append($("<label>")
			.append("Minimum")
			.append($("<input>")
				.attr("name", "min")
				.attr("type", "date")
			)
		);
		fieldset.append($("<label>")
			.append("Maximum")
			.append($("<input>")
				.attr("name", "max")
				.attr("type", "date")
			)
		);
		fieldset.append($("<label>")
			.append("Step")
			.append($("<input>")
				.attr("name", "step")
				.attr("type", "input")
			)
		);
		return fieldset;
	}

	renderInput() {
		let input = super.renderInput();
		if (this.min) input.attr("min", this.min);
		if (this.max) input.attr("max", this.max);
		if (this.step) input.attr("step", this.step);
		return input;
	}
}

class TimeQuestion extends NumericQuestion {
	constructor(name, label, required, list, min, max, step) {
		super(name, label, required, list);
		this.min = min;
		this.max = max;
		this.step = step * 1;
	}

	get type() { return "time"; }

	static deserialize(object) {
		return new TimeQuestion(object.name, object.label, object.required, object.list, object.min, object.max, object.step);
	}

	renderEditor() {
		let fieldset = super.renderEditor();
		fieldset.append($("<label>")
			.append("Minimum")
			.append($("<input>")
				.attr("name", "min")
				.attr("type", "time")
			)
		);
		fieldset.append($("<label>")
			.append("Maximum")
			.append($("<input>")
				.attr("name", "max")
				.attr("type", "time")
			)
		);
		fieldset.append($("<label>")
			.append("Step")
			.append($("<input>")
				.attr("name", "step")
				.attr("type", "input")
			)
		);
		return fieldset;
	}

	renderInput() {
		let input = super.renderInput();
		if (this.min) input.attr("min", this.min);
		if (this.max) input.attr("max", this.max);
		if (this.step) input.attr("step", this.step);
		return input;
	}
}

class RangeQuestion extends NumericQuestion {
	constructor(name, label, required, list, min, max, step) {
		super(name, label, required, list);
		this.min = min * 1;
		this.max = max * 1;
		this.step = step * 1;
	}

	get type() { return "range"; }

	static deserialize(object) {
		return new RangeQuestion(object.name, object.label, object.required, object.list, object.min, object.max, object.step);
	}

	renderEditor() {
		let fieldset = super.renderEditor();
		fieldset.append($("<label>")
			.append("Minimum")
			.append($("<input>")
				.attr("name", "min")
				.attr("type", "number")
			)
		);
		fieldset.append($("<label>")
			.append("Maximum")
			.append($("<input>")
				.prop("name", "max")
				.prop("type", "number")
			)
		);
		fieldset.append($("<label>")
			.append("Step")
			.append($("<input>")
				.attr("name", "step")
				.attr("type", "input")
			)
		);
		return fieldset;
	}

	renderInput() {
		let input = super.renderInput();
		if (this.min) input.attr("min", this.min);
		if (this.max) input.attr("max", this.max);
		if (this.step) input.attr("step", this.step);
		return input;
	}
}

class StringQuestion extends InputQuestion {
	constructor(name, label, required, list, minLength, maxLength, pattern) {
		super(name, label, required, list);
		this.minLength = minLength * 1;
		this.maxLength = maxLength * 1;
		this.pattern = pattern;
	}

	renderInput() {
		let input = super.renderInput();
		input.prop("name", this.name);
		if (this.minLength) input.attr("minLength", this.minLength);
		if (this.maxLength) input.attr("maxLength", this.maxLength);
		if (this.pattern) input.attr("pattern", this.pattern);
		return input;
	}

	renderEditor() {
		let fieldset = super.renderEditor();
		fieldset.append($("<label>")
			.append("Minimum length")
			.append($("<input>")
				.attr("name", "minlength")
				.attr("type", "number")
				.attr("min", "0")
			)
		);
		fieldset.append($("<label>")
			.append("Maximum length")
			.append($("<input>")
				.attr("name", "maxlength")
				.attr("type", "number")
				.attr("min", "0")
			)
		);
		fieldset.append($("<label>")
			.append("Validation pattern")
			.append($("<input>")
				.attr("name", "pattern")
				.attr("type", "text")
			)
		);
		return fieldset;
	}
}

class EmailQuestion extends StringQuestion {
	constructor(name, label, required, list, minLength, maxLength, pattern, multiple) {
		super(name, label, required, list, minLength, maxLength, pattern);
		this.multiple = multiple;
	}

	get type() { return "email"; }

	renderInput() {
		let input = super.renderInput();
		if (this.multiple) input.prop("multiple", this.multiple);
		return input;
	}

	renderEditor() {
		let fieldset = super.renderEditor();
		fieldset.append($("<label>")
			.append("Allow multiple")
			.append($("<input>")
				.attr("name", "multiple")
				.attr("type", "checkbox")
			)
		);
		return fieldset;
	}

	static deserialize(object) {
		return new EmailQuestion(object.name, object.label, object.required, object.list, object.minLength, object.maxLength, object.pattern, object.multiple);
	}
}

class TelQuestion extends StringQuestion {
	constructor(name, label, required, list, minLength, maxLength, pattern) {
		super(name, label, required, list, minLength, maxLength, pattern);
	}

	get type() { return "tel"; }

	static deserialize(object) {
		return new TelQuestion(object.name, object.label, object.required, object.list, object.minLength, object.maxLength, object.pattern);
	}

	renderEditor() {
		return super.renderEditor();
	}
}

class TextQuestion extends StringQuestion {
	constructor(name, label, required, list, minLength, maxLength, pattern) {
		super(name, label, required, list, minLength, maxLength, pattern);
	}

	get type() { return "text"; }

	static deserialize(object) {
		return new TextQuestion(object.name, object.label, object.required, object.list, object.minLength, object.maxLength, object.pattern);
	}

	renderEditor() {
		return super.renderEditor();
	}
}

let q = new TextQuestion("test", "test-l", false, null, null, null, null);
let sec = new Section([q], "Test section");
let s = new Survey([sec], []);
SurveyItem.deserialize(JSON.parse(JSON.stringify(s))).render()[0];
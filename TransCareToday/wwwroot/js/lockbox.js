﻿var hashFunction = "SHA-512";
var iterationCount = 250;
var saltLength = 16;
var ivLength = 16;
var modulusLength = 4096;
var publicExponent = new Uint8Array([0x01, 0x00, 0x01]);
var aesLength = 256;

function initializeLockbox(lockbox) {
	lockbox.passwordSalt = new Uint8Array(saltLength);
	crypto.getRandomValues(lockbox.passwordSalt);
	lockbox.keySalt = new Uint8Array(saltLength);
	crypto.getRandomValues(lockbox.keySalt);

	lockbox.passwordIterationCount = iterationCount;
	lockbox.keyIterationCount = iterationCount;

	lockbox.passwordHashFunction = hashFunction;
	lockbox.keyHashFunction = hashFunction;

	lockbox.decryptionKeyIv = new Uint8Array(ivLength);
	crypto.getRandomValues(lockbox.decryptionKeyIv);
	lockbox.signingKeyIv = new Uint8Array(ivLength);
	crypto.getRandomValues(lockbox.signingKeyIv);
	lockbox.symmetricKeyIv = new Uint8Array(ivLength);
	crypto.getRandomValues(lockbox.symmetricKeyIv);

	return lockbox;
}

function deriveKey(lockbox, password) {
	return crypto.subtle.importKey(
		"raw",
		new TextEncoder("utf-8").encode(password),
		{ name: "PBKDF2" },
		false,
		["deriveBits", "deriveKey"]
	).then(function (key) {
		return crypto.subtle.deriveKey(
			{
				"name": "PBKDF2",
				"salt": lockbox.keySalt,
				"iterations": lockbox.keyIterationCount,
				"hash": lockbox.keyHashFunction
			},
			key,
			{ "name": "AES-CBC", "length": aesLength },
			false,
			["wrapKey", "unwrapKey"]
		);
	});
}

function createLockbox(password) {
	let lockbox = initializeLockbox({});

	// Generate the wrapping key from the password.
	let derivedKeyPromise = deriveKey(lockbox, password);

	// Generate and wrap the encryption key pair.
	let encryptionKeyPairPromise = crypto.subtle.generateKey(
		{
			name: "RSA-OAEP",
			modulusLength: modulusLength,
			publicExponent: publicExponent,
			hash: hashFunction
		},
		true,
		["encrypt", "decrypt"]
	).then(function (keypair) {
		// Wrap the private key.
		let privatePromise = derivedKeyPromise.then(function (derivedKey) {
			return window.crypto.subtle.wrapKey(
				"pkcs8",
				keypair.privateKey,
				derivedKey,
				{
					name: "AES-CBC",
					iv: lockbox.decryptionKeyIv
				}
			).then(function (buffer) {
				lockbox.encryptedDecryptionKey = buffer;
			});
		});

		// Store the public key.
		let publicPromise = crypto.subtle.exportKey(
			"spki",
			keypair.publicKey
		).then(function (buffer) {
			lockbox.encryptionKey = buffer;
		});

		return Promise.all([privatePromise, publicPromise]);
	});

	// Generate and wrap the signing key pair.
	let signingKeyPairPromise = crypto.subtle.generateKey(
		{
			name: "RSA-PSS",
			modulusLength: modulusLength,
			publicExponent: publicExponent,
			hash: hashFunction
		},
		true,
		["sign", "verify"]
	).then(function (keypair) {
		// Wrap the private key.
		let privatePromise = derivedKeyPromise.then(function (derivedKey) {
			return window.crypto.subtle.wrapKey(
				"pkcs8",
				keypair.privateKey,
				derivedKey,
				{
					name: "AES-CBC",
					iv: lockbox.signingKeyIv
				}
			).then(function (buffer) {
				lockbox.encryptedSigningKey = buffer;
			});
		});

		// Store the public key.
		let publicPromise = crypto.subtle.exportKey(
			"spki",
			keypair.publicKey
		).then(function (buffer) {
			lockbox.verificationKey = buffer;
		});

		return Promise.all([privatePromise, publicPromise]);
	});

	// Generate and wrap the symmetric key.
	let symmetricKeyPromise = window.crypto.subtle.generateKey(
		{
			name: "AES-CBC",
			length: aesLength
		},
		true,
		["encrypt", "decrypt"]
	).then(function (key) {
		return derivedKeyPromise.then(function (derivedKey) {
			return window.crypto.subtle.wrapKey(
				"raw",
				key,
				derivedKey,
				{
					name: "AES-CBC",
					iv: lockbox.symmetricKeyIv
				}
			).then(function (buffer) {
				lockbox.encryptedSymmetricKey = buffer;
			});
		});
	});

	return Promise.all([encryptionKeyPairPromise, signingKeyPairPromise, symmetricKeyPromise]).then(function () {
		return lockbox;
	});
}

// Use this to hash the password before sending it to the server, otherwise the server would be able to reproduce the wrapping key.
function hashPassword(lockbox, password) {
	return crypto.subtle.importKey(
		"raw",
		new TextEncoder("utf-8").encode(password),
		{ name: "PBKDF2" },
		false,
		["deriveBits", "deriveKey"]
	).then(function (key) {
		return crypto.subtle.deriveKey(
			{
				"name": "PBKDF2",
				"salt": lockbox.passwordSalt,
				"iterations": lockbox.passwordIterationCount,
				"hash": lockbox.passwordHashFunction
			},
			key,
			{ "name": "AES-CBC", "length": aesLength },
			true,
			["encrypt", "decrypt"]
		);
	}).then(function (key) {
		return crypto.subtle.exportKey("raw", key);
	}).then(function (buffer) {
		return buffer;
	});
}

function getSymmetricKey(lockbox, password) {
	return deriveKey(lockbox, password).then(function (derivedKey) {
		return window.crypto.subtle.unwrapKey(
			"raw",
			lockbox.encryptedSymmetricKey,
			derivedKey,
			{
				name: "AES-CBC",
				iv: lockbox.symmetricKeyIv
			},
			{
				name: "AES-CBC",
				length: aesLength
			},
			false,
			["encrypt", "decrypt"]
		);
	}).catch(function () {
		throw 'Error fetching key; password incorrect.';
	});
}

function getDecryptionKey(lockbox, password) {
	return deriveKey(lockbox, password).then(function (derivedKey) {
		return window.crypto.subtle.unwrapKey(
			"pkcs8",
			lockbox.encryptedDecryptionKey,
			derivedKey,
			{
				name: "AES-CBC",
				iv: lockbox.decryptionKeyIv
			},
			{
				name: "RSA-OAEP",
				modulusLength: modulusLength,
				publicExponent: publicExponent,
				hash: hashFunction
			},
			false,
			["decrypt"]
		);
	}).catch(function () {
		throw 'Error fetching key; password incorrect.';
	});
}

function getSigningKey(lockbox, password) {
	return deriveKey(lockbox, password).then(function (derivedKey) {
		return window.crypto.subtle.unwrapKey(
			"pkcs8",
			lockbox.encryptedSigningKey,
			derivedKey,
			{
				name: "AES-CBC",
				iv: lockbox.signingKeyIv
			},
			{
				name: "RSA-PSS",
				modulusLength: modulusLength,
				publicExponent: publicExponent,
				hash: hashFunction
			},
			false,
			["sign"]
		);
	}).catch(function () {
		throw 'Error fetching key; password incorrect.';
	});
}

function getEncryptionKey(lockbox) {
	return window.crypto.subtle.importKey(
		"spki",
		lockbox.encryptionKey,
		{
			name: "RSA-OAEP",
			modulusLength: modulusLength,
			publicExponent: publicExponent,
			hash: hashFunction
		},
		false,
		["encrypt"]
	);
}

function getVerificationKey(lockbox) {
	return window.crypto.subtle.importKey(
		"spki",
		lockbox.verificationKey,
		{
			name: "RSA-PSS",
			modulusLength: modulusLength,
			publicExponent: publicExponent,
			hash: hashFunction
		},
		false,
		["verify"]
	);
}

function changeLockboxPassword(lockbox, oldPassword, newPassword) {
	// Clone the lockbox first.
	lockbox = Object.assign({}, lockbox);
	// Generate the old wrapping key from the old password.
	let oldKeyPromise = deriveKey(lockbox, oldPassword);

	// Decrypt the decryption key.
	let unwrapDecryptionKeyPromise = oldKeyPromise.then(function (derivedKey) {
		return window.crypto.subtle.unwrapKey(
			"pkcs8",
			lockbox.encryptedDecryptionKey,
			derivedKey,
			{
				name: "AES-CBC",
				iv: lockbox.decryptionKeyIv
			},
			{
				name: "RSA-OAEP",
				modulusLength: modulusLength,
				publicExponent: publicExponent,
				hash: hashFunction
			},
			true,
			["decrypt"]
		);
	});

	// Decrypt the signing key.
	let unwrapSigningKeyPromise = oldKeyPromise.then(function (derivedKey) {
		return window.crypto.subtle.unwrapKey(
			"pkcs8",
			lockbox.encryptedSigningKey,
			derivedKey,
			{
				name: "AES-CBC",
				iv: lockbox.signingKeyIv
			},
			{
				name: "RSA-PSS",
				modulusLength: modulusLength,
				publicExponent: publicExponent,
				hash: hashFunction
			},
			true,
			["sign"]
		);
	});

	// Decrypt the symmetric key.
	let unwrapSymmetricKeyPromise = oldKeyPromise.then(function (derivedKey) {
		return window.crypto.subtle.unwrapKey(
			"raw",
			lockbox.encryptedSymmetricKey,
			derivedKey,
			{
				name: "AES-CBC",
				iv: lockbox.symmetricKeyIv
			},
			{
				name: "AES-CBC",
				"length": aesLength
			},
			true,
			["encrypt", "decrypt"]
		);
	});

	let unwrappingPromise = Promise.all([unwrapDecryptionKeyPromise, unwrapSigningKeyPromise, unwrapSymmetricKeyPromise]).then(function () {
		lockbox = initializeLockbox(lockbox);
	});

	// Generate the new wrapping key from the new password.
	let newKeyPromise = unwrappingPromise.then(function () {
		return deriveKey(lockbox, newPassword);
	});

	// Wrap the decryption key.
	let wrapDecryptionKeyPromise = unwrapDecryptionKeyPromise.then(function (privateKey) {
		return newKeyPromise.then(function (derivedKey) {
			return window.crypto.subtle.wrapKey(
				"pkcs8",
				privateKey,
				derivedKey,
				{
					name: "AES-CBC",
					iv: lockbox.decryptionKeyIv
				}
			).then(function (buffer) {
				lockbox.encryptedDecryptionKey = buffer;
			});
		});
	});

	// Wrap the signing key.
	let wrapSigningKeyPromise = unwrapSigningKeyPromise.then(function (privateKey) {
		return newKeyPromise.then(function (derivedKey) {
			return window.crypto.subtle.wrapKey(
				"pkcs8",
				privateKey,
				derivedKey,
				{
					name: "AES-CBC",
					iv: lockbox.signingKeyIv
				}
			).then(function (buffer) {
				lockbox.encryptedSigningKey = buffer;
			});
		});
	});

	// Wrap the symmetric key.
	let wrapSymmetricKeyPromise = unwrapSymmetricKeyPromise.then(function (symmetricKey) {
		return newKeyPromise.then(function (derivedKey) {
			return window.crypto.subtle.wrapKey(
				"raw",
				symmetricKey,
				derivedKey,
				{
					name: "AES-CBC",
					iv: lockbox.symmetricKeyIv
				}
			).then(function (buffer) {
				lockbox.encryptedSymmetricKey = buffer;
			});
		});
	});

	return Promise.all([wrapDecryptionKeyPromise, wrapSigningKeyPromise, wrapSymmetricKeyPromise]).then(function () {
		return lockbox;
	});
}

function encrypt(lockbox, password, data) {
	return getEncryptionKey(lockbox).then(function (encryptionKey) {
		return crypto.subtle.encrypt({ "name": "RSA-OAEP" }, encryptionKey, data);
	});
}

function decrypt(lockbox, password, data) {
	return getDecryptionKey(lockbox, password).then(function (decryptionKey) {
		return crypto.subtle.decrypt({ "name": "RSA-OAEP" }, decryptionKey, data);
	});
}

function saveLockbox(lockbox) {
	localStorage.setItem("PasswordSalt", Base64.encode(lockbox.passwordSalt));
	localStorage.setItem("PasswordIterationCount", lockbox.passwordIterationCount);
	localStorage.setItem("PasswordHashFunction", lockbox.passwordHashFunction);

	localStorage.setItem("KeySalt", Base64.encode(lockbox.keySalt));
	localStorage.setItem("KeyIterationCount", lockbox.keyIterationCount);
	localStorage.setItem("KeyHashFunction", lockbox.keyHashFunction);

	localStorage.setItem("EncryptedDecryptionKey", Base64.encode(lockbox.encryptedDecryptionKey));
	localStorage.setItem("EncryptionKey", Base64.encode(lockbox.encryptionKey));
	localStorage.setItem("EncryptedSigningKey", Base64.encode(lockbox.encryptedSigningKey));
	localStorage.setItem("VerificationKey", Base64.encode(lockbox.verificationKey));
	localStorage.setItem("EncryptedSymmetricKey", Base64.encode(lockbox.encryptedSymmetricKey));

	localStorage.setItem("DecryptionKeyIv", Base64.encode(lockbox.decryptionKeyIv));
	localStorage.setItem("SigningKeyIv", Base64.encode(lockbox.signingKeyIv));
	localStorage.setItem("SymmetricKeyIv", Base64.encode(lockbox.symmetricKeyIv));
}


function clearLockbox() {
	localStorage.removeItem("PasswordSalt");
	localStorage.removeItem("PasswordIterationCount");
	localStorage.removeItem("PasswordHashFunction");

	localStorage.removeItem("KeySalt");
	localStorage.removeItem("KeyIterationCount");
	localStorage.removeItem("KeyHashFunction");

	localStorage.removeItem("EncryptedDecryptionKey");
	localStorage.removeItem("EncryptionKey");
	localStorage.removeItem("EncryptedSigningKey");
	localStorage.removeItem("VerificationKey");
	localStorage.removeItem("EncryptedSymmetricKey");

	localStorage.removeItem("DecryptionKeyIv");
	localStorage.removeItem("SigningKeyIv");
	localStorage.removeItem("SymmetricKeyIv");
}

function loadLockbox() {
	if (localStorage.getItem("PasswordSalt") === null) return null;
	if (localStorage.getItem("PasswordIterationCount") === null) return null;
	if (localStorage.getItem("PasswordHashFunction") === null) return null;

	if (localStorage.getItem("KeySalt") === null) return null;
	if (localStorage.getItem("KeyIterationCount") === null) return null;
	if (localStorage.getItem("KeyHashFunction") === null) return null;

	if (localStorage.getItem("EncryptedDecryptionKey") === null) return null;
	if (localStorage.getItem("EncryptionKey") === null) return null;
	if (localStorage.getItem("EncryptedSigningKey") === null) return null;
	if (localStorage.getItem("VerificationKey") === null) return null;
	if (localStorage.getItem("EncryptedSymmetricKey") === null) return null;

	if (localStorage.getItem("DecryptionKeyIv") === null) return null;
	if (localStorage.getItem("SigningKeyIv") === null) return null;
	if (localStorage.getItem("SymmetricKeyIv") === null) return null;

	let lockbox = {};
	lockbox.passwordSalt = Base64.decode(localStorage.getItem("PasswordSalt"));
	lockbox.passwordIterationCount = localStorage.getItem("PasswordIterationCount") * 1;
	lockbox.passwordHashFunction = localStorage.getItem("PasswordHashFunction");

	lockbox.keySalt = Base64.decode(localStorage.getItem("KeySalt"));
	lockbox.keyIterationCount = localStorage.getItem("KeyIterationCount") * 1;
	lockbox.keyHashFunction = localStorage.getItem("KeyHashFunction");

	lockbox.encryptedDecryptionKey = Base64.decode(localStorage.getItem("EncryptedDecryptionKey"));
	lockbox.encryptionKey = Base64.decode(localStorage.getItem("EncryptionKey"));
	lockbox.encryptedSigningKey = Base64.decode(localStorage.getItem("EncryptedSigningKey"));
	lockbox.verificationKey = Base64.decode(localStorage.getItem("VerificationKey"));
	lockbox.encryptedSymmetricKey = Base64.decode(localStorage.getItem("EncryptedSymmetricKey"));

	lockbox.decryptionKeyIv = Base64.decode(localStorage.getItem("DecryptionKeyIv"));
	lockbox.signingKeyIv = Base64.decode(localStorage.getItem("SigningKeyIv"));
	lockbox.symmetricKeyIv = Base64.decode(localStorage.getItem("SymmetricKeyIv"));
	return lockbox;
}

function parseChallenge(jsonObject) {
	let challenge = {};
	challenge.passwordSalt = Base64.decode(jsonObject.passwordSalt);
	challenge.passwordIterationCount = jsonObject.passwordIterationCount * 1;
	challenge.passwordHashFunction = jsonObject.passwordHashFunction;
	return challenge;
}

function parseLockbox(jsonObject) {
	let lockbox = {};
	lockbox.passwordSalt = Base64.decode(jsonObject.passwordSalt);
	lockbox.passwordIterationCount = jsonObject.passwordIterationCount * 1;
	lockbox.passwordHashFunction = jsonObject.passwordHashFunction;

	lockbox.keySalt = Base64.decode(jsonObject.keySalt);
	lockbox.keyIterationCount = jsonObject.keyIterationCount * 1;
	lockbox.keyHashFunction = jsonObject.keyHashFunction;

	lockbox.encryptedDecryptionKey = Base64.decode(jsonObject.encryptedDecryptionKey);
	lockbox.encryptionKey = Base64.decode(jsonObject.encryptionKey);
	lockbox.encryptedSigningKey = Base64.decode(jsonObject.encryptedSigningKey);
	lockbox.verificationKey = Base64.decode(jsonObject.verificationKey);
	lockbox.encryptedSymmetricKey = Base64.decode(jsonObject.encryptedSymmetricKey);

	lockbox.decryptionKeyIv = Base64.decode(jsonObject.decryptionKeyIv);
	lockbox.signingKeyIv = Base64.decode(jsonObject.signingKeyIv);
	lockbox.symmetricKeyIv = Base64.decode(jsonObject.symmetricKeyIv);
	return lockbox;
}

//var lockbox;
//var lockboxPromise = createLockbox("password").then(lb => lockbox = lb);
//var encryptPromise = lockboxPromise.then(function (lockbox) {
//	return encrypt(lockbox, "password", new TextEncoder().encode("secret"));
//});
//var decryptPromise = encryptPromise.then(function (secret) {
//	return decrypt(lockbox, "password", secret).then(data => new TextDecoder().decode(data));
//});
//var changePasswordPromise = Promise.all([encryptPromise, decryptPromise]).then(function () {
//	return changeLockboxPassword(lockbox, "password", "password2");
//});
//var successfulDecryptPromise = changePasswordPromise.then(function (lockbox) {
//	return encryptPromise.then(function (secret) {
//		return decrypt(lockbox, "password2", secret).then(data => new TextDecoder().decode(data));
//	});
//});
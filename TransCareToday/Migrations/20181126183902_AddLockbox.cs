﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TransCareToday.Migrations
{
    public partial class AddLockbox : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Lockbox",
                columns: table => new
                {
                    LockboxId = table.Column<Guid>(nullable: false),
                    PasswordSalt = table.Column<string>(nullable: false),
                    PasswordIterationCount = table.Column<long>(nullable: false),
                    PasswordHashFunction = table.Column<string>(nullable: false),
                    KeySalt = table.Column<string>(nullable: false),
                    KeyIterationCount = table.Column<long>(nullable: false),
                    KeyHashFunction = table.Column<string>(nullable: false),
                    EncryptedDecryptionKey = table.Column<string>(nullable: false),
                    EncryptionKey = table.Column<string>(nullable: false),
                    EncryptedSigningKey = table.Column<string>(nullable: false),
                    VerificationKey = table.Column<string>(nullable: false),
                    EncryptedSymmetricKey = table.Column<string>(nullable: false),
                    DecryptionKeyIv = table.Column<string>(nullable: false),
                    SigningKeyIv = table.Column<string>(nullable: false),
                    SymmetricKeyIv = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lockbox", x => x.LockboxId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Lockbox");
        }
    }
}

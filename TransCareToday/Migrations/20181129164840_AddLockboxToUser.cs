﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TransCareToday.Migrations
{
    public partial class AddLockboxToUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "LockboxId",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_LockboxId",
                table: "AspNetUsers",
                column: "LockboxId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Lockbox_LockboxId",
                table: "AspNetUsers",
                column: "LockboxId",
                principalTable: "Lockbox",
                principalColumn: "LockboxId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Lockbox_LockboxId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_LockboxId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "LockboxId",
                table: "AspNetUsers");
        }
    }
}

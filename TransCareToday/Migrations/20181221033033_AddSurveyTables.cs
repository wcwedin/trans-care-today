﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NpgsqlTypes;

namespace TransCareToday.Migrations
{
	public partial class AddSurveyTables : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropForeignKey(
				name: "FK_AspNetUsers_Lockbox_LockboxId",
				table: "AspNetUsers");

			migrationBuilder.DropPrimaryKey(
				name: "PK_Lockbox",
				table: "Lockbox");

			migrationBuilder.RenameTable(
				name: "Lockbox",
				newName: "Lockboxes");

			migrationBuilder.AddPrimaryKey(
				name: "PK_Lockboxes",
				table: "Lockboxes",
				column: "LockboxId");

			migrationBuilder.CreateTable(
				name: "Providers",
				columns: table => new
				{
					ProviderId = table.Column<Guid>(nullable: false),
					IsInactive = table.Column<bool>(nullable: false),
					IsAcceptingNewPatients = table.Column<bool>(nullable: false),
					Name = table.Column<string>(nullable: false),
					Description = table.Column<string>(nullable: false),
					AddressLine1 = table.Column<string>(nullable: false),
					AddressLine2 = table.Column<string>(nullable: true),
					AddressLine3 = table.Column<string>(nullable: true),
					City = table.Column<string>(nullable: false),
					State = table.Column<string>(type: "varchar(2)", nullable: false),
					ZipCode = table.Column<string>(nullable: false),
					SearchVector = table.Column<NpgsqlTsVector>(nullable: true)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Providers", x => x.ProviderId);
				});

			migrationBuilder.CreateTable(
				name: "PatientUsers",
				columns: table => new
				{
					PatientUserId = table.Column<Guid>(nullable: false),
					UserId = table.Column<string>(nullable: false),
					ProviderId = table.Column<Guid>(nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_PatientUsers", x => x.PatientUserId);
					table.UniqueConstraint("AK_PatientUsers_ProviderId_UserId", x => new { x.ProviderId, x.UserId });
					table.ForeignKey(
						name: "FK_PatientUsers_Providers_ProviderId",
						column: x => x.ProviderId,
						principalTable: "Providers",
						principalColumn: "ProviderId",
						onDelete: ReferentialAction.Cascade);
					table.ForeignKey(
						name: "FK_PatientUsers_AspNetUsers_UserId",
						column: x => x.UserId,
						principalTable: "AspNetUsers",
						principalColumn: "Id",
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.CreateTable(
				name: "StaffUsers",
				columns: table => new
				{
					PatientUserId = table.Column<Guid>(nullable: false),
					UserId = table.Column<string>(nullable: false),
					ProviderId = table.Column<Guid>(nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_StaffUsers", x => x.PatientUserId);
					table.UniqueConstraint("AK_StaffUsers_UserId", x => x.UserId);
					table.ForeignKey(
						name: "FK_StaffUsers_Providers_ProviderId",
						column: x => x.ProviderId,
						principalTable: "Providers",
						principalColumn: "ProviderId",
						onDelete: ReferentialAction.Cascade);
					table.ForeignKey(
						name: "FK_StaffUsers_AspNetUsers_UserId",
						column: x => x.UserId,
						principalTable: "AspNetUsers",
						principalColumn: "Id",
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.CreateTable(
				name: "Surveys",
				columns: table => new
				{
					SurveyId = table.Column<Guid>(nullable: false),
					ProviderId = table.Column<Guid>(nullable: false),
					Name = table.Column<string>(nullable: false),
					IsHidden = table.Column<bool>(nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Surveys", x => x.SurveyId);
					table.UniqueConstraint("AK_Surveys_SurveyId_ProviderId", x => new { x.SurveyId, x.ProviderId });
					table.ForeignKey(
						name: "FK_Surveys_Providers_ProviderId",
						column: x => x.ProviderId,
						principalTable: "Providers",
						principalColumn: "ProviderId",
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.CreateTable(
				name: "SurveyVersions",
				columns: table => new
				{
					SurveyVersionId = table.Column<Guid>(nullable: false),
					CreatedByUserId = table.Column<string>(nullable: false),
					Questions = table.Column<string>(nullable: false),
					SurveyId = table.Column<Guid>(nullable: false),
					CreatedAt = table.Column<DateTime>(nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_SurveyVersions", x => x.SurveyVersionId);
					table.UniqueConstraint("AK_SurveyVersions_SurveyId_CreatedAt", x => new { x.SurveyId, x.CreatedAt });
					table.UniqueConstraint("AK_SurveyVersions_SurveyVersionId_SurveyId", x => new { x.SurveyVersionId, x.SurveyId });
					table.ForeignKey(
						name: "FK_SurveyVersions_AspNetUsers_CreatedByUserId",
						column: x => x.CreatedByUserId,
						principalTable: "AspNetUsers",
						principalColumn: "Id",
						onDelete: ReferentialAction.Cascade);
					table.ForeignKey(
						name: "FK_SurveyVersions_Surveys_SurveyId",
						column: x => x.SurveyId,
						principalTable: "Surveys",
						principalColumn: "SurveyId",
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.CreateTable(
				name: "ActiveSurveys",
				columns: table => new
				{
					ActiveSurveyId = table.Column<Guid>(nullable: false),
					ProviderId = table.Column<Guid>(nullable: false),
					SurveyId = table.Column<Guid>(nullable: false),
					SurveyVersionId = table.Column<Guid>(nullable: false),
					Position = table.Column<int>(nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_ActiveSurveys", x => x.ActiveSurveyId);
					table.UniqueConstraint("AK_ActiveSurveys_ProviderId_Position", x => new { x.ProviderId, x.Position });
					table.ForeignKey(
						name: "FK_ActiveSurveys_Providers_ProviderId",
						column: x => x.ProviderId,
						principalTable: "Providers",
						principalColumn: "ProviderId",
						onDelete: ReferentialAction.Cascade);
					table.ForeignKey(
						name: "FK_ActiveSurveys_Surveys_SurveyId_ProviderId",
						columns: x => new { x.SurveyId, x.ProviderId },
						principalTable: "Surveys",
						principalColumns: new[] { "SurveyId", "ProviderId" },
						onDelete: ReferentialAction.Cascade);
					table.ForeignKey(
						name: "FK_ActiveSurveys_SurveyVersions_SurveyVersionId_SurveyId",
						columns: x => new { x.SurveyVersionId, x.SurveyId },
						principalTable: "SurveyVersions",
						principalColumns: new[] { "SurveyVersionId", "SurveyId" },
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.CreateTable(
				name: "SurveyResponses",
				columns: table => new
				{
					SurveyResponseId = table.Column<Guid>(nullable: false),
					UserId = table.Column<string>(nullable: false),
					SurveyId = table.Column<Guid>(nullable: false),
					SurveyVersionId = table.Column<Guid>(nullable: false),
					ProviderId = table.Column<Guid>(nullable: false),
					Answers = table.Column<string>(nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_SurveyResponses", x => x.SurveyResponseId);
					table.ForeignKey(
						name: "FK_SurveyResponses_Providers_ProviderId",
						column: x => x.ProviderId,
						principalTable: "Providers",
						principalColumn: "ProviderId",
						onDelete: ReferentialAction.Cascade);
					table.ForeignKey(
						name: "FK_SurveyResponses_AspNetUsers_UserId",
						column: x => x.UserId,
						principalTable: "AspNetUsers",
						principalColumn: "Id",
						onDelete: ReferentialAction.Cascade);
					table.ForeignKey(
						name: "FK_SurveyResponses_Surveys_SurveyId_ProviderId",
						columns: x => new { x.SurveyId, x.ProviderId },
						principalTable: "Surveys",
						principalColumns: new[] { "SurveyId", "ProviderId" },
						onDelete: ReferentialAction.Cascade);
					table.ForeignKey(
						name: "FK_SurveyResponses_SurveyVersions_SurveyVersionId_SurveyId",
						columns: x => new { x.SurveyVersionId, x.SurveyId },
						principalTable: "SurveyVersions",
						principalColumns: new[] { "SurveyVersionId", "SurveyId" },
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.CreateIndex(
				name: "IX_ActiveSurveys_SurveyId_ProviderId",
				table: "ActiveSurveys",
				columns: new[] { "SurveyId", "ProviderId" },
				unique: true);

			migrationBuilder.CreateIndex(
				name: "IX_ActiveSurveys_SurveyVersionId_SurveyId",
				table: "ActiveSurveys",
				columns: new[] { "SurveyVersionId", "SurveyId" },
				unique: true);

			migrationBuilder.CreateIndex(
				name: "IX_PatientUsers_UserId",
				table: "PatientUsers",
				column: "UserId");

			migrationBuilder.CreateIndex(
				name: "IX_Providers_SearchVector",
				table: "Providers",
				column: "SearchVector")
				.Annotation("Npgsql:IndexMethod", "GIN");

			migrationBuilder.CreateIndex(
				name: "IX_StaffUsers_ProviderId",
				table: "StaffUsers",
				column: "ProviderId");

			migrationBuilder.CreateIndex(
				name: "IX_SurveyResponses_ProviderId",
				table: "SurveyResponses",
				column: "ProviderId");

			migrationBuilder.CreateIndex(
				name: "IX_SurveyResponses_UserId",
				table: "SurveyResponses",
				column: "UserId");

			migrationBuilder.CreateIndex(
				name: "IX_SurveyResponses_SurveyId_ProviderId",
				table: "SurveyResponses",
				columns: new[] { "SurveyId", "ProviderId" });

			migrationBuilder.CreateIndex(
				name: "IX_SurveyResponses_SurveyVersionId_SurveyId",
				table: "SurveyResponses",
				columns: new[] { "SurveyVersionId", "SurveyId" });

			migrationBuilder.CreateIndex(
				name: "IX_Surveys_ProviderId",
				table: "Surveys",
				column: "ProviderId");

			migrationBuilder.CreateIndex(
				name: "IX_SurveyVersions_CreatedByUserId",
				table: "SurveyVersions",
				column: "CreatedByUserId");

			migrationBuilder.AddForeignKey(
				name: "FK_AspNetUsers_Lockboxes_LockboxId",
				table: "AspNetUsers",
				column: "LockboxId",
				principalTable: "Lockboxes",
				principalColumn: "LockboxId",
				onDelete: ReferentialAction.Cascade);

			migrationBuilder.Sql(
				@"CREATE TRIGGER provider_search_vector_update BEFORE INSERT OR UPDATE
				ON ""Providers"" FOR EACH ROW EXECUTE PROCEDURE
				tsvector_update_trigger(""SearchVector"", 'pg_catalog.english',
				""Name"", ""Description"", ""AddressLine1"", ""AddressLine2"", ""AddressLine3"", ""City"", ""State"", ""ZipCode"");"
			);
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.Sql(@"DROP TRIGGER provider_search_vector_update ON ""Providers"";");

			migrationBuilder.DropForeignKey(
				name: "FK_AspNetUsers_Lockboxes_LockboxId",
				table: "AspNetUsers");

			migrationBuilder.DropTable(
				name: "ActiveSurveys");

			migrationBuilder.DropTable(
				name: "PatientUsers");

			migrationBuilder.DropTable(
				name: "StaffUsers");

			migrationBuilder.DropTable(
				name: "SurveyResponses");

			migrationBuilder.DropTable(
				name: "SurveyVersions");

			migrationBuilder.DropTable(
				name: "Surveys");

			migrationBuilder.DropTable(
				name: "Providers");

			migrationBuilder.DropPrimaryKey(
				name: "PK_Lockboxes",
				table: "Lockboxes");

			migrationBuilder.RenameTable(
				name: "Lockboxes",
				newName: "Lockbox");

			migrationBuilder.AddPrimaryKey(
				name: "PK_Lockbox",
				table: "Lockbox",
				column: "LockboxId");

			migrationBuilder.AddForeignKey(
				name: "FK_AspNetUsers_Lockbox_LockboxId",
				table: "AspNetUsers",
				column: "LockboxId",
				principalTable: "Lockbox",
				principalColumn: "LockboxId",
				onDelete: ReferentialAction.Cascade);
		}
	}
}

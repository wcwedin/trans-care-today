﻿namespace TransCareToday.ViewModels.TableView
{
	public class FilterColumn
	{
		public string Column { get; set; }
		public FilterOperator Operator { get; set; }
		public object Value { get; set; }
	}
}
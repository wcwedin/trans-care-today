﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace TransCareToday.ViewModels.TableView
{
	public class TableView<T> : List<T>, ITableViewParameters
	{
		public int PageIndex { get; }
		public int PageSize { get; }
		public IEnumerable<SortColumn> SortColumns { get; }
		public IEnumerable<FilterColumn> FilterColumns { get; }

		public int TotalRows { get; }

		public int TotalPages => (int)Math.Ceiling(TotalRows / (double)PageSize);
		public bool HasPreviousPage => (PageIndex > 1);
		public bool HasNextPage => (PageIndex < TotalPages);

		public TableView(TableViewParameters parameters, int totalRows)
		{
			PageIndex = parameters.PageIndex;
			PageSize = parameters.PageSize;
			SortColumns = parameters.SortColumns.ToList();
			FilterColumns = parameters.FilterColumns.ToList();
			TotalRows = totalRows;
		}

		public async Task Apply(IQueryable<T> source)
		{
			var list = await source.ToListAsync();
			AddRange(list);
		}
	}
}
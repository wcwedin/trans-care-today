﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransCareToday.ViewModels.TableView
{
	public class TableViewColumn
	{
		public ITableViewParameters Parameters { get; set; }
		public string Column { get; set; }
		public string Display { get; set; }

		public TableViewColumn() { }

		public TableViewColumn(ITableViewParameters parameters, string column, string display)
		{
			Parameters = parameters;
			Column = column;
			Display = display;
		}
	}
}

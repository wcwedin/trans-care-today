﻿using System.Collections.Generic;

namespace TransCareToday.ViewModels.TableView
{
	public class TableViewParameters : ITableViewParameters
	{
		public int PageIndex { get; set; }
		public int PageSize { get; set; }
		public List<SortColumn> SortColumns { get; } = new List<SortColumn>();
		public List<FilterColumn> FilterColumns { get; } = new List<FilterColumn>();

		IEnumerable<SortColumn> ITableViewParameters.SortColumns => SortColumns;
		IEnumerable<FilterColumn> ITableViewParameters.FilterColumns => FilterColumns;
	}
}
﻿using System.Collections.Generic;

namespace TransCareToday.ViewModels.TableView
{
	public interface ITableViewParameters
	{
		int PageIndex { get; }
		int PageSize { get; }
		IEnumerable<SortColumn> SortColumns { get; }
		IEnumerable<FilterColumn> FilterColumns { get; }
	}
}
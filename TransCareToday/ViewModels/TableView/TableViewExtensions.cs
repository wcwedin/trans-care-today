﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransCareToday.ViewModels.TableView
{
	public static class TableViewExtensions
	{
		public static List<SortColumn> SortBy(this IEnumerable<SortColumn> columns, string column)
		{
			var result = new List<SortColumn>(columns);
			var first = columns.FirstOrDefault();
			result.RemoveAll(c => c.Column == column);

			var descending = first != null && first.Column == column
				? !first.Descending
				: false;

			result.Insert(0, new SortColumn { Column = column, Descending = descending });

			return result;
		}
	}
}

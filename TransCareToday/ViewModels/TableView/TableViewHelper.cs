﻿using Microsoft.EntityFrameworkCore;
using NpgsqlTypes;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace TransCareToday.ViewModels.TableView
{
	public static class TableViewHelper
	{
		public static async Task<TableView<T>> ToTableView<T>(this IQueryable<T> source, TableViewParameters parameters)
		{
			var (result, totalRows) = await source.Filter(parameters).Sort(parameters).Paginate(parameters);
			var tableView = new TableView<T>(parameters, totalRows);
			await tableView.Apply(result);
			return tableView;
		}

		public static IQueryable<T> Filter<T>(this IQueryable<T> source, ITableViewParameters parameters)
		{
			if (!parameters.FilterColumns.Any())
				return source;
			foreach (var column in parameters.FilterColumns)
			{
				source = FilterByProperty(source, column.Column, column.Operator, column.Value);
			}
			return source;
		}

		public static IQueryable<T> Sort<T>(this IQueryable<T> source, ITableViewParameters parameters)
		{
			if (!parameters.SortColumns.Any())
				return source;
			foreach (var column in parameters.SortColumns)
			{
				source = OrderByProperty(source, column.Column, column.Descending);
			}
			return source;
		}

		public static async Task<(IQueryable<T> page, int rows)> Paginate<T>(this IQueryable<T> source, ITableViewParameters parameters)
		{
			var rows = await source.CountAsync();
			var items = source
				.Skip((parameters.PageIndex - 1) * parameters.PageSize)
				.Take(parameters.PageSize);
			return (items, rows);
		}

		public static IQueryable<T> FilterByProperty<T>(this IQueryable<T> source, string propertyName, FilterOperator op, object value)
		{
			if (typeof(T).GetProperty(propertyName, BindingFlags.IgnoreCase |
				BindingFlags.Public | BindingFlags.Instance) == null)
			{
				return null;
			}

			ParameterExpression parameterExpression = Expression.Parameter(typeof(T));
			Expression propertyExpression = Expression.Property(parameterExpression, propertyName);
			Expression constantExpression = Expression.Constant(value);

			Expression comparison = null;
			switch (op)
			{
				case FilterOperator.Equals:
					comparison = Expression.Equal(propertyExpression, constantExpression);
					break;
				case FilterOperator.Matches:
					var toString = constantExpression.Type.GetMethods().Single(m => m.Name == "ToString" && m.GetParameters().Length == 0);
					var toStringCall = Expression.Call(constantExpression, toString);
					var matchesMethod = typeof(NpgsqlFullTextSearchLinqExtensions).GetMethods().Single(m => m.Name == "Matches" && m.GetParameters()[1].ParameterType == typeof(string));
					comparison = Expression.Call(matchesMethod, propertyExpression, toStringCall);
					break;
			}

			var method = typeof(Queryable).GetMethods().First(m => m.Name == "Where" && m.GetParameters().Length == 2);

			MethodInfo genericMethod = method.MakeGenericMethod(typeof(T));
			LambdaExpression lambda = Expression.Lambda(comparison, parameterExpression);

			return (IQueryable<T>)genericMethod.Invoke(null, new object[] { source, lambda });
		}

		public static IOrderedQueryable<T> OrderByProperty<T>(this IQueryable<T> source, string propertyName, bool descending)
		{
			if (typeof(T).GetProperty(propertyName, BindingFlags.IgnoreCase |
				BindingFlags.Public | BindingFlags.Instance) == null)
			{
				return null;
			}

			string methodName;
			if (source is IOrderedQueryable<T>)
			{
				var lastMethod = (source.Expression as MethodCallExpression)?.Method;

				if (lastMethod?.DeclaringType == typeof(Queryable))
				{
					switch (lastMethod.Name)
					{
						case nameof(Queryable.OrderBy):
						case nameof(Queryable.OrderByDescending):
						case nameof(Queryable.ThenBy):
						case nameof(Queryable.ThenByDescending):
							methodName = descending ? "ThenByDescending" : "ThenBy";
							break;
						default:
							methodName = descending ? "OrderByDescending" : "OrderBy";
							break;
					}
				}
				else
				{
					methodName = descending ? "OrderByDescending" : "OrderBy";
				}
			}
			else
			{
				methodName = descending ? "OrderByDescending" : "OrderBy";
			}

			var method = typeof(Queryable).GetMethods().Single(m => m.Name == methodName && m.GetParameters().Length == 2);

			ParameterExpression parameterExpression = Expression.Parameter(typeof(T));
			Expression orderByProperty = Expression.Property(parameterExpression, propertyName);
			LambdaExpression lambda = Expression.Lambda(orderByProperty, parameterExpression);
			MethodInfo genericMethod = method.MakeGenericMethod(typeof(T), orderByProperty.Type);
			return (IOrderedQueryable<T>)genericMethod.Invoke(null, new object[] { source, lambda });
		}
	}
}
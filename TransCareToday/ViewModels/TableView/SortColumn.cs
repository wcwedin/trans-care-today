﻿namespace TransCareToday.ViewModels.TableView
{
	public class SortColumn
	{
		public string Column { get; set; }
		public bool Descending { get; set; }
	}
}
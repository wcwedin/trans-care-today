using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace TransCareToday.Models
{
	public class TransCareTodayContext : IdentityDbContext<User>
	{
		public TransCareTodayContext(DbContextOptions<TransCareTodayContext> options)
			: base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			builder.Entity<Provider>()
				.HasIndex(p => p.SearchVector)
				.ForNpgsqlHasMethod("GIN");

			builder.Entity<SurveyVersion>()
				.HasAlternateKey(sv => new { sv.SurveyId, sv.CreatedAt });

			builder.Entity<ActiveSurvey>()
				.HasAlternateKey(s => new { s.ProviderId, s.Position });

			builder.Entity<StaffUser>()
				.HasAlternateKey(su => new { su.UserId });

			builder.Entity<PatientUser>()
				.HasAlternateKey(pu => new { pu.ProviderId, pu.UserId });

			builder.Entity<User>()
				.HasOne(u => u.StaffUser)
				.WithOne(su => su.User)
				.HasForeignKey<StaffUser>(s => new { s.UserId })
				.HasPrincipalKey<User>(u => new { u.Id });

			builder.Entity<Survey>()
				.HasOne(s => s.ActiveSurvey)
				.WithOne(s => s.Survey)
				.HasForeignKey<ActiveSurvey>(s => new { s.SurveyId, s.ProviderId })
				.HasPrincipalKey<Survey>(s => new { s.SurveyId, s.ProviderId });

			builder.Entity<SurveyVersion>()
				.HasOne(sv => sv.ActiveSurvey)
				.WithOne(s => s.SurveyVersion)
				.HasForeignKey<ActiveSurvey>(s => new { s.SurveyVersionId, s.SurveyId })
				.HasPrincipalKey<SurveyVersion>(sv => new { sv.SurveyVersionId, sv.SurveyId });

			builder.Entity<SurveyVersion>()
				.HasMany(sv => sv.SurveyResponses)
				.WithOne(sr => sr.SurveyVersion)
				.HasForeignKey(sr => new { sr.SurveyVersionId, sr.SurveyId })
				.HasPrincipalKey(sv => new { sv.SurveyVersionId, sv.SurveyId });

			builder.Entity<Survey>()
				.HasMany(s => s.SurveyResponses)
				.WithOne(sr => sr.Survey)
				.HasForeignKey(sr => new { sr.SurveyId, sr.ProviderId })
				.HasPrincipalKey(s => new { s.SurveyId, s.ProviderId });

			base.OnModelCreating(builder);
		}

		public DbSet<ActiveSurvey> ActiveSurveys { get; set; }
		public DbSet<Lockbox> Lockboxes { get; set; }
		public DbSet<PatientUser> PatientUsers { get; set; }
		public DbSet<Provider> Providers { get; set; }
		public DbSet<StaffUser> StaffUsers { get; set; }
		public DbSet<Survey> Surveys { get; set; }
		public DbSet<SurveyResponse> SurveyResponses { get; set; }
		public DbSet<SurveyVersion> SurveyVersions { get; set; }
	}
}

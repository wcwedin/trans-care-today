using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using TransCareToday.Models;

[assembly: HostingStartup(typeof(TransCareToday.Areas.Identity.IdentityHostingStartup))]
namespace TransCareToday.Areas.Identity
{
	public class IdentityHostingStartup : IHostingStartup
	{
		public void Configure(IWebHostBuilder builder)
		{
			builder.ConfigureServices((context, services) =>
			{
				services.AddDefaultIdentity<User>().AddEntityFrameworkStores<TransCareTodayContext>();
			});
		}
	}
}
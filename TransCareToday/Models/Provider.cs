﻿using Microsoft.EntityFrameworkCore;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TransCareToday.Models
{
	public class Provider
	{
		[Key]
		public Guid ProviderId { get; set; }

		public ICollection<Survey> Surveys { get; set; }
		public ICollection<ActiveSurvey> ActiveSurveys { get; set; }
		public ICollection<StaffUser> StaffUsers { get; set; }
		public ICollection<PatientUser> PatientUsers { get; set; }

		[Required]
		public bool IsInactive { get; set; }
		[Required]
		public bool IsAcceptingNewPatients { get; set; }
		[Required]
		public string Name { get; set; }
		[Required]
		public string Description { get; set; }

		[Required]
		public string AddressLine1 { get; set; }
		public string AddressLine2 { get; set; }
		public string AddressLine3 { get; set; }
		[Required]
		public string City { get; set; }
		[Required]
		[Column(TypeName = "varchar(2)")]
		public State State { get; set; }
		[Required]
		public string ZipCode { get; set; }

		public NpgsqlTsVector SearchVector { get; set; }
	}
}







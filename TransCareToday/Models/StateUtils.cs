﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace TransCareToday.Models
{
	public static class StateUtils
	{
		private static Dictionary<State, string> stateNames = new Dictionary<State, string>();

		static StateUtils()
		{
			stateNames[State.AL] = "Alabama";
			stateNames[State.AK] = "Alaska";
			stateNames[State.AS] = "American Samoa";
			stateNames[State.AZ] = "Arizona";
			stateNames[State.AR] = "Arkansas";
			stateNames[State.CA] = "California";
			stateNames[State.CO] = "Colorado";
			stateNames[State.CT] = "Connecticut";
			stateNames[State.DE] = "Delaware";
			stateNames[State.DC] = "District of Columbia";
			stateNames[State.FM] = "Federated States of Micronesia";
			stateNames[State.FL] = "Florida";
			stateNames[State.GA] = "Georgia";
			stateNames[State.GU] = "Guam";
			stateNames[State.HI] = "Hawaii";
			stateNames[State.ID] = "Idaho";
			stateNames[State.IL] = "Illinois";
			stateNames[State.IN] = "Indiana";
			stateNames[State.IA] = "Iowa";
			stateNames[State.KS] = "Kansas";
			stateNames[State.KY] = "Kentucky";
			stateNames[State.LA] = "Louisiana";
			stateNames[State.ME] = "Maine";
			stateNames[State.MH] = "Marshall Islands";
			stateNames[State.MD] = "Maryland";
			stateNames[State.MA] = "Massachusetts";
			stateNames[State.MI] = "Michigan";
			stateNames[State.MN] = "Minnesota";
			stateNames[State.MS] = "Mississippi";
			stateNames[State.MO] = "Missouri";
			stateNames[State.MT] = "Montana";
			stateNames[State.NE] = "Nebraska";
			stateNames[State.NV] = "Nevada";
			stateNames[State.NH] = "New Hampshire";
			stateNames[State.NJ] = "New Jersey";
			stateNames[State.NM] = "New Mexico";
			stateNames[State.NY] = "New York";
			stateNames[State.NC] = "North Carolina";
			stateNames[State.ND] = "North Dakota";
			stateNames[State.MP] = "Northern Mariana Islands";
			stateNames[State.OH] = "Ohio";
			stateNames[State.OK] = "Oklahoma";
			stateNames[State.OR] = "Oregon";
			stateNames[State.PW] = "Palau";
			stateNames[State.PA] = "Pennsylvania";
			stateNames[State.PR] = "Puerto Rico";
			stateNames[State.RI] = "Rhode Island";
			stateNames[State.SC] = "South Carolina";
			stateNames[State.SD] = "South Dakota";
			stateNames[State.TN] = "Tennessee";
			stateNames[State.TX] = "Texas";
			stateNames[State.UT] = "Utah";
			stateNames[State.VT] = "Vermont";
			stateNames[State.VI] = "Virgin Islands";
			stateNames[State.VA] = "Virginia";
			stateNames[State.WA] = "Washington";
			stateNames[State.WV] = "West Virginia";
			stateNames[State.WI] = "Wisconsin";
			stateNames[State.WY] = "Wyoming";
		}

		public static string GetName(this State state)
		{
			return stateNames[state];
		}

		public static IReadOnlyDictionary<State, string> StateNames
		{
			get
			{
				return new ReadOnlyDictionary<State, string>(stateNames);
			}
		}
	}
}







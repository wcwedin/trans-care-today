﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransCareToday.Models
{
	public class SurveyVersion
	{
		[Key]
		public Guid SurveyVersionId { get; set; }
		public ICollection<SurveyResponse> SurveyResponses { get; set; }

		public User CreatedByUser { get; set; }
		[ForeignKey("CreatedByUser")]
		[Required]
		public string CreatedByUserId { get; set; }

		[Required]
		public string Questions { get; set; }

		public Survey Survey { get; set; }
		[Required]
		public Guid SurveyId { get; set; }

		public ActiveSurvey ActiveSurvey { get; set; }
		[Required]
		public DateTime CreatedAt { get; set; }
	}
}

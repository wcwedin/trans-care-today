﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TransCareToday.Models
{
	public class Survey
	{
		[Key]
		public Guid SurveyId { get; set; }

		public Provider Provider { get; set; }
		[Required]
		public Guid ProviderId { get; set; }

		public ActiveSurvey ActiveSurvey { get; set; }
		public ICollection<SurveyVersion> SurveyVersions { get; set; }
		public ICollection<SurveyResponse> SurveyResponses { get; set; }

		[Required]
		public string Name { get; set; }
		[Required]
		public bool IsHidden { get; set; }
	}
}

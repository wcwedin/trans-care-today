﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TransCareToday.Models
{
	public class SurveyResponse
	{
		[Key]
		public Guid SurveyResponseId { get; set; }

		public User User { get; set; }
		[Required]
		public string UserId { get; set; }

		public Survey Survey { get; set; }
		[Required]
		public Guid SurveyId { get; set; }

		public SurveyVersion SurveyVersion { get; set; }
		[Required]
		public Guid SurveyVersionId { get; set; }

		public Provider Provider { get; set; }
		[Required]
		public Guid ProviderId { get; set; }

		[Required]
		public string Answers { get; set; }
	}
}

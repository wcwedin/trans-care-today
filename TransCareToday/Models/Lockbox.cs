﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TransCareToday.Models
{
    public class Lockbox
	{
		[Key]
		public Guid LockboxId { get; set; }
		[Required]
		public string PasswordSalt { get; set; }
		[Required]
		public uint PasswordIterationCount { get; set; }
		[Required]
		public string PasswordHashFunction { get; set; }

		[Required]
		public string KeySalt { get; set; }
		[Required]
		public uint KeyIterationCount { get; set; }
		[Required]
		public string KeyHashFunction { get; set; }

		[Required]
		public string EncryptedDecryptionKey { get; set; }
		[Required]
		public string EncryptionKey { get; set; }
		[Required]
		public string EncryptedSigningKey { get; set; }
		[Required]
		public string VerificationKey { get; set; }
		[Required]
		public string EncryptedSymmetricKey { get; set; }

		[Required]
		public string DecryptionKeyIv { get; set; }
		[Required]
		public string SigningKeyIv { get; set; }
		[Required]
		public string SymmetricKeyIv { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace TransCareToday.Models
{
	// Add profile data for application users by adding properties to the User class
	public class User : IdentityUser
	{
		public Lockbox Lockbox { get; set; }
		[Required]
		public Guid LockboxId { get; set; }
		public StaffUser StaffUser { get; set; }
		public ICollection<PatientUser> PatientUsers { get; set; }
		public ICollection<SurveyResponse> SurveyResponses { get; set; }
	}
}

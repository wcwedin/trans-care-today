﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TransCareToday.Models
{
	public class ActiveSurvey
	{
		[Key]
		public Guid ActiveSurveyId { get; set; }

		public Provider Provider { get; set; }
		[Required]
		public Guid ProviderId { get; set; }

		public Survey Survey { get; set; }
		[Required]
		public Guid SurveyId { get; set; }

		public SurveyVersion SurveyVersion { get; set; }
		[Required]
		public Guid SurveyVersionId { get; set; }

		public int Position { get; set; }
	}
}

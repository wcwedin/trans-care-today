﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TransCareToday.Models
{
	public class PatientUser
	{
		[Key]
		public Guid PatientUserId { get; set; }

		public User User { get; set; }
		[Required]
		public string UserId { get; set; }

		public Provider Provider { get; set; }
		[Required]
		public Guid ProviderId { get; set; }
	}
}
